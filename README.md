# spacewalk-remote-utils [![](https://images.microbadger.com/badges/version/jorgeandrada/spacewalk-remote-utils.svg)](https://microbadger.com/images/jorgeandrada/spacewalk-remote-utils "Get your own version badge on microbadger.com")[![](https://images.microbadger.com/badges/image/jorgeandrada/spacewalk-remote-utils.svg)](https://microbadger.com/images/jorgeandrada/spacewalk-remote-utils "Get your own image badge on microbadger.com")[![](https://images.microbadger.com/badges/commit/jorgeandrada/spacewalk-remote-utils.svg)](https://microbadger.com/images/jorgeandrada/spacewalk-remote-utils "Get your own commit badge on microbadger.com")

Centos with spacewalk-remote-utils for sync channels in spacewalk


**More info:** [spacewalkproject](https://spacewalkproject.github.io/)
